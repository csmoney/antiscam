There is an AntiScam chrome extension which prevents entrance to fishing sites and api-key theft by scammers

Main extension functional:
1. When the fake site, that is in the black list, is acessed, the entire content of the page is deleted and instead of it a page appears with a warning that this site is fraudulent.
2. If the user has the cs.money tab or steamcommunity.com open, the extension loads the https://steamcommunity.com/dev/apikey page in the background and checks for the "revoke" button in the DOM. If it is present, then your account has an api key, which means your account might be under the threat of theft. In this case, the extension shows the user a window with information about how to get rid of the effects of hacking. Please note that the extension does not save your API key and does not even access it explicitly.
3. The extension has the ability to report a fraudulent site. Our Scam Police team will check the site for fraud.