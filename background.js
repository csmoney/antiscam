const notificationBase = {
    type: "basic",
    title: "",
    message: "",
    iconUrl: "images/icon48.png"
}

chrome.runtime.onInstalled.addListener(details => {
    if (details.reason == "install") {
        chrome.storage.local.set({ language: "en" })
        chrome.storage.local.set({
            settings: {
                preventScamSites: true,
                preventApiScam: true,
                liveAnalysis: true
            }
        })
        chrome.storage.local.set({ theme: "dark" })

        fetch(config.hostname + "/list_scam_sites")
            .then(response => response.json())
            .then(data => {
                if (data) {
                    chrome.storage.local.set({ scamSites: data })

                    initialAnalysis()
                }
            })
    }
})

function initialAnalysis() {
    chrome.tabs.getAllInWindow((tabs) => {
        tabs.forEach(tab => {
            const whitelisted =
                config.whitelist.findIndex(
                    website => tab.url.startsWith(website),
                    tab.url
                ) == -1
                    ? false
                    : true
                
            if (whitelisted) {
                notificationBase.title = "CS.Money Official"
                notificationBase.message =
                    "This is the original CS.Money site. It's safe to trade here."

                chrome.notifications.create("officialWebsite", notificationBase)

                chrome.storage.local.get("settings", res => {
                    const settings = res.settings

                    if (settings.preventApiScam) {
                        fetch('https://steamcommunity.com/dev/apikey')
                            .then(response => response.text())
                            .then(data => {
                                let nt = document.createElement('div')
                                nt.innerHTML = data

                                if (!nt.querySelector('#editForm input')) {
                                    chrome.storage.local.set({'hasAPIKey': false})
                                }
                                
                                if (nt.querySelector('#editForm input').type === 'submit') {
                                    chrome.storage.local.set({'hasAPIKey': true})
                                    window.open("alerts/main.html")
                                } else {
                                    chrome.storage.local.set({'hasAPIKey': false})
                                }
                            })
                    }
                })
            }
        })
    })
}

chrome.alarms.create("getScamSites", {
    when: Date.now() + 1000,
    periodInMinutes: 30
})
chrome.alarms.create("checkAPIKey", {
    when: Date.now() + 1000,
    periodInMinutes: 30
})

chrome.alarms.onAlarm.addListener(function(alarm) {
    switch (alarm.name) {
        case "getScamSites":
            fetch(config.hostname + "/list_scam_sites")
                .then(response => response.json())
                .then(data => {
                    if (data) {
                        chrome.storage.local.set({ scamSites: data })
                    }
                })

            break

        case "checkAPIKey":
            checkAPIKeyStatus(res => {
                console.log(`API-key: ${res ? "Not protected" : "Protected"}`)
            })

            break

        default:
            break
    }
})

chrome.webNavigation.onCompleted.addListener(event => {
    if (event.frameId === 0) {
        chrome.tabs.get(event.tabId, tab => {
            const whitelisted =
                config.whitelist.findIndex(
                    website => tab.url.startsWith(website),
                    tab.url
                ) == -1
                    ? false
                    : true

            if (whitelisted) {
                notificationBase.title = "CS.Money Official"
                notificationBase.message =
                    "This is the original CS.Money site. It's safe to trade here."

                chrome.notifications.create("officialWebsite", notificationBase)

                chrome.storage.local.get("settings", res => {
                    const settings = res.settings

                    if (settings.preventApiScam) {
                        chrome.storage.local.get("hasAPIKey", res => {
                            const hasAPIKey = res.hasAPIKey

                            if (hasAPIKey) {
                                window.open("alerts/main.html")
                            }
                        })
                    }
                })
            } else if (
                tab.url.startsWith("https://steamcommunity.com/tradeoffer")
            ) {
                chrome.storage.local.get("settings", res => {
                    const settings = res.settings

                    if (settings.preventApiScam) {
                        checkAPIKeyStatus(res => {
                            const hasAPIKey = res

                            if (hasAPIKey) {
                                chrome.tabs.insertCSS(
                                    tab.id,
                                    { file: "css/tradeoffer.css" },
                                    () => {
                                        chrome.tabs.executeScript(tab.id, {
                                            file: `injection_files/tradeOfferInjector.js`
                                        })
                                    }
                                )
                            }
                        })
                    }
                })
            } else {
                chrome.storage.local.get("settings", res => {
                    const settings = res.settings

                    if (settings.preventScamSites) {
                        chrome.storage.local.get("scamSites", res => {
                            const scamSites = res.scamSites

                            scamSites.forEach(site => {
                                if (tab.url.includes(site)) {
                                    chrome.tabs.insertCSS(
                                        tab.id,
                                        { file: "css/scampage.css" },
                                        () => {
                                            chrome.tabs.executeScript(tab.id, {
                                                file:
                                                    "injection_files/scamSiteInjector.js"
                                            })
                                        }
                                    )
                                }
                            })
                        })
                    }

                    if (settings.liveAnalysis) {
                        chrome.tabs.insertCSS(
                            tab.id,
                            { file: "css/livealert.css" },
                            () => {
                                chrome.tabs.executeScript(tab.id, {
                                    file: "injection_files/LiveAnalysis.js"
                                })
                            }
                        )
                    }
                })
            }
        })
    }
})

function checkAPIKeyStatus(cb) {
    fetch("https://steamcommunity.com/dev/apikey")
        .then(response => response.text())
        .then(data => {
            let nt = document.createElement("div")
            nt.innerHTML = data

            if (
                nt.querySelector("#editForm") &&
                nt.querySelector("#editForm input").type === "submit"
            ) {
                chrome.storage.local.set({ hasAPIKey: true })
                cb(true)
            } else {
                chrome.storage.local.set({ hasAPIKey: false })
                cb(false)
            }
        })
}
