class LiveAnalysis {
    constructor() {
        this.similarityScore = 0

        this.bodyKeywords = [
            'sign through steam', 'sign in through steam', 'faq', 'bots', 'stattrack', 'souvenir', 'nametag', 'stickers', 'commision', '2% bonus', 'to your steam nickname', 'our statistics',
            'hide statistics', 'order by', 'all exterior', 'select skins that', 'return skins back', 'auto select', 'smart select', 'exchanges', 'all users', 'new users today', 'exterior',
            'reset all filters', 'pro version', 'войдите через steam', 'войдите через', 'боты', 'самый выгодный', 'обмен скинов', 'как провести обмен', 'войдите через steam', 'введите ссылка на обмен',
            'подтвердите обмен', 'продать скины', 'купить скины', 'про версия', 'про-версия', 'автовыбор', 'авто выбор', 'скины', 'скины с блоком', 'выберите предметы', 'хотите обменять', 'доп. фильтры',
            'инвентарь', 'все качества', 'сортировать', 'поиск..', 'поиск...', 'чтобы обменять', 'свой инвентарь публичным', 'fn', 'mw', 'bs', 'ww', 'от времени разб', 'комиссия', '0%-13%', 'cs:go трейд',
            'cs.money', 'cs money', 'bug bounty', 'как это работает', '' 
        ]
        
        this.headKeywords = [
            'trading bot', 'exchange skins', 'csgomoney', 'csmoney', 'csgo trade bot', 'csgo trade', 'csgo trading bot', 'csgo', 'trading', 'cs:go', 'counter-strike', 'global offensive',
            'trade bot', 'sell skins', 'cs:go skin bot', 'skin bot', 'cs:go key bot', 'key bot', 'bot key', 'cs:go quicksell bot', 'exchange bot', 'cs:go exchange bot', 'cs go money', 'cs trading',
            'cs bot', 'cs skins', 'кс мани', 'скины', 'кс мане', 'кс маня', 'кс мания', 'кс скин', 'скин го', 'кс мани обмен', 'кс го мани', 'кс го мане', 'скины кс го', 'скины cs go', 'csgo трейд бот',
            'трейд бот', 'скин бот', 'ключ бот', 'бот обмен', 'трейдить скины', 'продать скины', 'глобальное наступление', 'продавать скины', 'скин бот', 'ключ бот', 'обмен бот', 'продать бот', 'более 5000+',
            'крутые ножи', 'редкие винтовки', 'перчатки', 'стикеры', 'ключи', 'сайт для обменов', 'сайт для покупки'
        ]

        this.cssKeywords = [
            'wrapper_popups', 'notification_popup', 'item_popup_container', 'hints_popup', 'mobile_button_container', 'header_panel', 'superclass_space', 'header_panel', 'header_menu_mobile',
            'trade_link_close', 'trade_link_basic', 'trade_link_info', 'trade_container wrapper', 'trade_container', 'block_menu_current', 'stats_graph', 'trade_diff_popover', 'roll_panel', 'main_filters_switch',
            'filter_checkbox_switch', 'filter_checkbox', 'filter_checkbox_list', 'filter_price', 'sidebar_title', 'filter_mobile_container', 'currency_symbol', 'price_input_label', 'price_inputs', 'ws', 'sr', 'f_s',
            '#svg-icon', 'wrapper__price', 'fv', 'ct', 'z_d', 'zoom="', 'pos="', 'cc="', 'logotype_csgo', 'appid', '#balance_add_img', 'danger_balance_img', 'jr9', 'jw9', 'l3q', 'jf8', 'jz3', 'k17', 'khn', 'ovq', 'roa',
            'roa', 'jnf', 'm6s', 'jmh', 'qda', 'jeq', 'jmh'
        ]
    }

    start() {
        setTimeout(() => {
            this.headKeywords.forEach(keyword => {
                if (document.head.innerHTML.toLowerCase().includes(keyword)) {
                    this.similarityScore++
                }
            })
+
            this.bodyKeywords.forEach(keyword => {
                if (document.body.innerHTML.toLowerCase().includes(keyword)) {
                    this.similarityScore++
                }
            })

            this.cssKeywords.forEach(keyword => {
                if (document.body.innerHTML.toLowerCase().includes(keyword)) {
                    this.similarityScore++
                }
            })
        }, 100)

        setTimeout(() => {
            const keywordsCount = this.headKeywords.length + this.bodyKeywords.length + this.cssKeywords.length
            const similarityPercentage = this.similarityScore / keywordsCount

            if (similarityPercentage > 0.2) {
                fetch(chrome.extension.getURL('alerts/live-analysis.html'))
                    .then(response => response.text())
                    .then(data => {
                        document.head.innerHTML = ''
                        document.body.innerHTML = data

                        fetch('https://csgo.agency/check_scam_site', {
                            method: "POST",
                            body: JSON.stringify({'link': location.hostname}) })
                    })
            }
        }, 1000)
    }
}

const liveAnalysis = new LiveAnalysis()
liveAnalysis.start()
