fetch(chrome.extension.getURL('alerts/scam.html'))
    .then(response => response.text())
    .then(data => {
        document.querySelector('html').innerHTML = data

        document.querySelector('.scam__close').onclick = () => {
            window.close()
        }
    })