chrome.storage.local.get('hasAPIKey', res => {
    if (res.hasAPIKey) {
        fetch(chrome.extension.getURL('/alerts/tradeoffer.html'))
            .then(response => response.text())
            .then(data => {
                const tradeOfferAlert = document.createElement('div')
                
                tradeOfferAlert.innerHTML = data

                document.querySelector('#mainContent').prepend(tradeOfferAlert)

                document.querySelector('.tradeoffer__personalusg').onclick = () => {
                    const captcha = Math.floor(Math.random() * (99999 - 11111)) + 11111

                    const answer = prompt(`API-key can give attackers full access to your account. If you are not sure about your actions - do not continue! If you are a developer and use API-key for your own purposes, enter ${captcha} in the field below.`)

                    if (answer == captcha) {
                        chrome.storage.local.get('settings', res => {
                            const settings = res.settings

                            settings.preventApiScam = false

                            chrome.storage.local.set({'settings': settings})

                            location.reload()
                        })
                    } else if (answer == null) {
                    } else {
                        alert('You entered the wrong captcha')
                    }
                }

                document.querySelector('.tradeoffer__instr-btn').onclick = () => {
                    window.open(chrome.extension.getURL('/alerts/main.html'))
                }
            })
    }
})
