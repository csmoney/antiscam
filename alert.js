const personalUsageButton = document.querySelector('.alert__personalus')

chrome.storage.local.get('settings', res => {
    const settings = res.settings

    if (settings.preventApiScam) {
        personalUsageButton.onclick = () => {
            const captcha = Math.floor(Math.random() * (99999 - 11111)) + 11111
            const answer = prompt(`API-key can give attackers full access to your account. If you are not sure about your actions - do not continue! If you are a developer and use API-key for your own purposes, enter ${captcha} in the field below.`)
        
            if (answer == captcha) {
                chrome.storage.local.get('settings', res => {
                    const settings = res.settings
        
                    settings.preventApiScam = false
        
                    chrome.storage.local.set({'settings': settings})
                    
                    chrome.tabs.getCurrent((tab) => {
                        chrome.tabs.remove(tab.id, () => { })
                    })

                    personalUsageButton.textContent = 'Prevent API Scam disabled'
                    personalUsageButton.style.color = 'rgba(255, 255, 255, .4)'
                    personalUsageButton.style.pointerEvents = 'none'
                })
            } else if (answer == null) {
            } else {
                alert('You entered the wrong captcha')
            }
        }
    } else {
        personalUsageButton.style.display = 'none'
    }
})
